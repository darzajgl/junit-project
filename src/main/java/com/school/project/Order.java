package com.school.project;

import java.util.ArrayList;
import java.util.List;

public class Order {


    private List<Meal> meals = new ArrayList<>();

    public void addMealToOrder(Meal mealToAdd){
        meals.add(mealToAdd);
    }

    public void addMealToOrder(List<Meal> mealList1) {
        meals.addAll(mealList1);
    }

    public void removeMealFromOrder(Meal mealToRemove){
        if (meals.contains(mealToRemove))
        meals.remove(mealToRemove);

    }

    public List<Meal> getMeals() {
        return meals;
    }


}
