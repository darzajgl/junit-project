package com.school.project;

public class User {

    private String name;
    private int age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name.length() > 5) {
            this.name = name;
        } else {
            throw new IllegalArgumentException();
        }
    }


    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        if (age < 15) {
            throw new IllegalArgumentException();
        }
        this.age = age;
    }
}

