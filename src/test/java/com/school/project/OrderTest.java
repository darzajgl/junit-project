package com.school.project;

import org.junit.jupiter.api.Test;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.*;

class OrderTest {


    //just an example
    @Test
    void testAssertArrayEquals() {
        //given
        int[] ints1 = {1, 2, 3};
        int[] ints2 = {1, 2, 3};

        //then
        assertArrayEquals(ints1, ints2);
    }

    @Test
    void mealListShouldBeEmptyAfterCreationOfOrder() {

        //given
        Order order = new Order();

        //then
        assertThat(order.getMeals(), empty());
        assertThat(order.getMeals().size(), equalTo(0));
        assertThat(order.getMeals(), hasSize(0));
        assertThat(order.getMeals(), emptyCollectionOf(Meal.class));


    }

    @Test
    void addingMealToTheOrderShouldIncreaseOrderSize() {
        //given
        Order order1 = new Order();
        Meal meal1 = new Meal(12, "Kebab");

        //when
        order1.addMealToOrder(meal1);

        //then
        assertThat(order1.getMeals().size(), equalTo(1));
        assertThat(order1.getMeals(), hasSize(1));
    }

    @Test
    void addingMealToOrderShouldIncreaseOrderListSize() {
        Meal meal1 = new Meal(12, "Kebs");
        Meal meal2 = new Meal(100, "Pizza");

        Order order1 = new Order();
        order1.addMealToOrder(meal1);


        assertThat(order1.getMeals(), contains(meal1));

        assertThat(order1.getMeals(), hasItem(meal1));

        assertThat(order1.getMeals().get(0).getPrice(), equalTo(12));

    }

    @Test
    void addingAndRemovingMealToTheOrderShouldNotChangeOrderSize() {
        //given
        Order order1 = new Order();
        Meal meal1 = new Meal(12, "Kebab");

        //when
        order1.addMealToOrder(meal1);
        order1.removeMealFromOrder(meal1);

        //then
        assertThat(order1.getMeals().size(), equalTo(0));
        //or
        assertThat(order1.getMeals(), hasSize(0));
        assertThat(order1.getMeals(), not(contains(meal1)));
    }

    @Test
    void mealsShouldBeInCorrectOrderAfterAddingThemToOrder() {
        Meal meal1 = new Meal(12, "Kebs");
        Meal meal2 = new Meal(100, "Pizza");
        Meal meal3 = new Meal(10, "Pizza2222");

        Order order1 = new Order();
        order1.addMealToOrder(meal1);
        order1.addMealToOrder(meal2);
//        order1.addMealToOrder(meal3);

//        assertThat(order1.getMeals(), contains(meal2,meal1)); //test failed - wrong order of arguments
        assertThat(order1.getMeals(), hasItem(meal1));
        assertThat(order1.getMeals(), hasItems(meal2, meal1));

        assertThat(order1.getMeals(), containsInAnyOrder(meal2, meal1));

    }

    @Test
    void testIfTwoOrdersAreTheSame() {
        Meal meal1 = new Meal(12, "Kebs");
        Meal meal2 = new Meal(50, "Pizza");
        Meal meal3 = new Meal(19, "Pierogi");

        List<Meal> mealList1 = Arrays.asList(meal1, meal2);
        List<Meal> mealList2 = Arrays.asList(meal1, meal2);
        List<Meal> mealList3 = Arrays.asList(meal2, meal1);
        List<Meal> mealList4 = Arrays.asList(meal1, meal2, meal3);
        Order order1 = new Order();
        Order order2 = new Order();

        order1.addMealToOrder(mealList1);
        order2.addMealToOrder(mealList1);
//        order2.addMealToOrder(mealList3); // homework - make this scenario past test

        assertThat(order1.getMeals(), is(order2.getMeals()));


    }

}