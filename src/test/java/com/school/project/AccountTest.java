package com.school.project;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.*;

class AccountTest {

    @Test
    void newlyCreatedAccountShouldNotBeActive() {
        // given
        Account account = new Account();

        //then
        assertFalse(account.isActive(),
                "Check if new account is not active");


        assertThat(account.isActive(), equalTo(false));
        assertThat(account.isActive(), is(false));
    }


    @Test
    void newlyCreatedAccountShouldBeActiveAfterActivation() {
        //given
        Account account = new Account();

        //when
        account.activate();

        //then
        assertTrue(account.isActive(),
                "Check if new account is not active");

    }

    @Test
    void newlyCreatedAccountShouldHaveDefaultDeliveryAddressEqualToNull() {

        //1
//        //given
//        Account account = new Account();
//        //when
//        Address address = account.getDefaultDeliverAddress();
//        //then
//        assertNull(address);

        // 2
        //given
        Account account1 = new Account();

        //then
        assertNull(account1.getDefaultDeliverAddress());
        assertThat(account1.getDefaultDeliverAddress(), (nullValue()));
    }

    @Test
    void deliveryAddressShouldNotBeNullAfterBeingSetInAccount() {

        // 2
        //given
        Account account1 = new Account();

        //when
        account1.setDefaultDeliverAddress(new Address("Szkolna", "100a"));

        //then
        assertNotNull(account1.getDefaultDeliverAddress());
        assertThat(account1.getDefaultDeliverAddress(), notNullValue());

    }

    @Test
    void givenEmailAddressShouldBeNotNull() {
        //given
        Account account1 = new Account();

        //when
        account1.setEmailAddress("james@gmial.com");

        //then
        assertThat(account1.getEmailAddress(), notNullValue());

    }

    @Test
    void givenEmailAddressWithoutAtShouldThrowException() {

        //given
        Account account1 = new Account();

        //then
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            account1.setEmailAddress("elo.123");
        });
    }

    @Test
    void givenEmailAddressIsToShortShouldThrowException() {

        //given
        Account account1 = new Account();

        //when

        //then
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            account1.setEmailAddress("elaaaaaaaaaaaaaaaaaao.123");
        });
    }


    @ParameterizedTest
    @ValueSource(strings = {"laaaao@123.com", "helloaaaaaaaaaaa@123.com", "12aaaaaaaaaaaaa3@hell.com"})
    void givenEmailAddressShouldBeNotNull(String string) {
        //given
        Account account1 = new Account();

        //when
        account1.setEmailAddress(string);

        //then
        assertThat(account1.getEmailAddress(), notNullValue());
    }


    @Test
    void givenEmailAddressIsToShortShouldReturnException() {
        Account account1 = new Account();

        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            account1.setEmailAddress(("ga@wp.pl"));
        });


    }


}