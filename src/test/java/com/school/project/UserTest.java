package com.school.project;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.jupiter.api.Assertions.*;

class UserTest {


    @Test
    void emptyNameShouldBeEqualNull() {
        User user1 = new User();
        assertNull(user1.getName());
    }

    @Test
    void ageUnder15ShouldThrowException() {
        User user2 = new User();
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            user2.setAge(14);
        });

    }

    @ParameterizedTest()
    @ValueSource(strings = {"1", "AB", "098"})
    void givenTooShortNameShouldThrowException(String names) {
        User user3 = new User();
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            user3.setName(names);
        });
    }
}

