package com.cschool.example;

import com.cschool.example.SimpleCalculator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class SimpleCalculatorTest {

    @Test
    void add() {
        Assertions.assertEquals(4,SimpleCalculator.add(2,2));
//        assertEquals(4,com.cschool.example.SimpleCalculator.add(2,2));
    }

//    @Disabled
    @Test
    void multiply() {
        Assertions.assertEquals(6,SimpleCalculator.multiply(3,2));
    }


    @Test
    public void method1(){
        Assertions.assertEquals(5, SimpleCalculator.divide(10,2));
    }


    @Test
    public void method2(){

        Assertions.assertThrows(IllegalArgumentException.class, () ->{
            SimpleCalculator.divide(10,0);
        });
    }



}